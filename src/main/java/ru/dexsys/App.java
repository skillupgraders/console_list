package ru.dexsys;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Logger;

/**
 * Приложение, которое выводит на экран три списка:
 * числа, которые можно поделить без остатка на 3
 * числа, которые можно поделить без остатка на 7
 * числа, которые можно поделить без остатка на 21
 *
 */
public class App 
{
    private Logger logger = Logger.getLogger(App.class.getName());
    private List<Integer> div3 = new ArrayList<>();
    private List<Integer> div7 = new ArrayList<>();
    private List<Integer> div21 = new ArrayList<>();

    public static void main( String[] args )
    {
        App app = new App();
        if (args.length == 0) {
            app.readFromConsole();
        } else {
            app.fillArrays(Arrays.asList(args));
        }
        app.logToConsole();
    }

    private void readFromConsole() {
        logger.info("Введите целые числа через пробел: ");
        Scanner in = new Scanner(System.in);
        String consoleString = in.nextLine();
        List<String> argsList = Arrays.asList(consoleString.split("\\s"));
        fillArrays(argsList);
    }

    private void fillArrays(List<String> args) {
        for (String stringArgument : args) {
            int doubleArgument;
            try {
                doubleArgument = Integer.parseInt(stringArgument);
                if (doubleArgument % 3 == 0) {
                    div3.add(doubleArgument);
                }
                if (doubleArgument % 7 == 0) {
                    div7.add(doubleArgument);
                }
                if (doubleArgument % 21 == 0) {
                    div21.add(doubleArgument);
                }
            } catch (NumberFormatException nfe) {
                logger.info("Должны быть введены только целые числа");
                nfe.printStackTrace();
            }
        }
    }

    private void logToConsole() {
        logger.info("Числа которые можно поделить без остатка на 3: " + div3.toString());
        logger.info("Числа которые можно поделить без остатка на 7: " + div7.toString());
        logger.info("Числа которые можно поделить без остатка на 21: " + div21.toString());
    }
}
